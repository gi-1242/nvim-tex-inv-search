# Script and NeoVim Plugin to do LaTeX Forward / Inverse Searches

This repository has moved to [codeberg](https://codeberg.org/gi1242/nvim-tex-inv-search).

    https://codeberg.org/gi1242/nvim-tex-inv-search
